FROM oraclelinux:7.9
# Base image download

MAINTAINER satish
# Information about whoever created this Dockerfile

RUN yum install java -y
# Install httpd inside a container
# RUN instruction is used to execute any linux commands

WORKDIR /var/www/html
# set the container working directory or worksapce

COPY . .
# Copy all code from Host Operating system current directory to Container current working directory

EXPOSE 80
# Information to the user whoever is using this Dockerfile to create containers

ENTRYPOINT ["httpd","-DFOREGROUND"]
# To start apache server (we use systemctl start httpd to start httpd in VM)
# CMD or ENTRYPOINT is used to specify Parent Process for a container
# if you are not mentioning Parent process in Dockerfile then Docker will take Base Image default Parent process


